<?php
$CONFIG = array (
  'htaccess.RewriteBase' => '/',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => 'ocscaahocf4q',
  'passwordsalt' => 'H7MUK5DTOyzuakYDMixUGk7CjQGsvf',
  'secret' => 'bkHaxICpbZNyElqELsZtyvRSWTqbkN8TZICOygETZWGeqeak',
  'trusted_domains' => 
  array (
    0 => 'localhost:32771',
  ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'sqlite3',
  'version' => '17.0.1.1',
  'overwrite.cli.url' => 'http://localhost:32771',
  'installed' => true,
);
