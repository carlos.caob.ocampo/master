<?php 
$OC_Version = array(17,0,1,1);
$OC_VersionString = '17.0.1';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '16.0' => true,
    '17.0' => true,
  ),
  'owncloud' => 
  array (
  ),
);
$OC_Build = '2019-11-07T12:06:23+00:00 a8405bf175d9924309b3889948b8fb26aa707fd6';
$vendor = 'nextcloud';
